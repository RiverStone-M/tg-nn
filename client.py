import time
import sys
import logging
import threading
from stun import *
from netPeerFinder import *
from netPeerTests import *
from customTest import *

logging.basicConfig(level=logging.DEBUG)


class interface():
    def __init__(self,
                 autoMode,
                 serverPORT,
                 serverIP,
                 myPort,
                 useStun,
                 defaultAutoTest):
        self.autoMode = autoMode
        self.serverPORT = serverPORT
        self.serverIP = serverIP
        self.myPort = myPort
        self.useStun = useStun
        self.defaultAutoTest = defaultAutoTest
        self.usingCustomTest = False

    def run(self):

        self.setupStun()

        self.configTest()

        try:
            self.startTesting()
            self.testFinished()

        except Exception as e:
            print(e)

        print("Quiting Program")

    def setupStun(self):
        if self.useStun:
            try:
                stunInfoString = getExtAddress()
                stunInfoTemp = stunInfoString.split("\n")
                self.stunInfo = list()
                self.stunInfo.append(
                    stunInfoTemp[0].strip().split(":")[1].strip())
                self.stunInfo.append(
                    stunInfoTemp[1].strip().split(":")[1].strip())
                self.stunInfo.append(
                    stunInfoTemp[2].strip().split(":")[1].strip())
            except:
                self.stunInfo = ["Undetermined", '1', '1']
        else:
            self.stunInfo = ["Undetermined", '1', '1']

    def configTest(self):
        if self.autoMode == False:
            self.askTestFromUser()
        else:
            print(f"Starting automatic test")
            self.testChosen = self.defaultAutoTest

    def askTestFromUser(self):
        print("Please choose one of the options(type the number):")
        print("1)PingPong Test")
        print("2)BigFile Test")
        print("3)Custom Test")
        print("4)quit")
        testChosen = input("$")
        try:
            testChosen = int(testChosen)
            if testChosen == 1:
                self.testChosen = "PingPong"
            elif testChosen == 2:
                self.testChosen = "iperf"
            elif testChosen == 3:
                self.testChosen = customTestName
                self.usingCustomTest = True
            else:
                print("Exiting program")
                exit()
        except Exception as e:
            print(e)


    def startTesting(self):
        test = testRunner(self.serverPORT, self.serverIP, self.myPort,
                          self.stunInfo[0], self.testChosen,
                          self.usingCustomTest)
        test.run()


    def testFinished(self):
        print("Testing finished")


class testRunner():
    def __init__(self, serverPORT, serverIP, myPort, NATtype, testName,
                 usingCustomTest):
        self.serverPORT = serverPORT
        self.serverIP = serverIP
        self.myPort = myPort
        self.NATtype = NATtype

        if usingCustomTest:
            self.testName = "custom"
            self.realTestName = testName
        else:
            self.testName = testName
            self.realTestName = testName

    def run(self):
        logging.debug("Starting connection with peerfinder")

        sock = setupSock(self.myPort)
        try:
            PeerFinderConnect(sock, self.serverIP, self.serverPORT,
                              self.NATtype, self.realTestName)
            peerInfo = findPeer(sock)
        except Exception as e:
            print(e)
            raise Exception("Peerfinder error")
        finally:
            logging.debug(f"socket {sock} sendo fechado")
            sock.close()


        logging.debug(f"Trying to connect with {peerInfo}")

        testThread = threading.Thread(
            target=PeerCommunication,
            args=(peerInfo['ipPublic'], self.myPort,
                  [peerInfo['pairIP'], peerInfo["pairPORT"]], self.testName),
            daemon=True)

        testThread.start()

        testThread.join(timeout=4 * 3600)


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="Client side for P2P NN testing")
    parser.add_argument('-a',
                        "--auto",
                        action="store_true",
                        default=False,
                        help="Enables automatic testing with any test",
                        dest="autoMode")
    parser.add_argument('-sp',
                        "--serverPort",
                        default=12345,
                        help="port used by the peerFinder server",
                        dest="serverPort")
    parser.add_argument('-sip',
                        "--serverIP",
                        default=socket.gethostbyname(socket.gethostname()),
                        help="peerFinder server's ip",
                        dest="serverIP")
    parser.add_argument("-mp",
                        "--myPort",
                        type=int,
                        default=11111,
                        help="Your testing port",
                        dest="myPort")
    parser.add_argument("-s",
                        "--stun",
                        action="store_true",
                        default=False,
                        help="Enables pyStun to Find your NAT type",
                        dest="stunOn")
    parser.add_argument('-da',
                        "--AutoTestName",
                        default="PingPong",
                        help="Auto test used in auto mode",
                        dest="defaultAutoTest")

    args = parser.parse_args()

    app = interface(args.autoMode, args.serverPort, args.serverIP, args.myPort,
                    args.stunOn, args.defaultAutoTest)
    app.run()


if __name__ == '__main__':
    main()
