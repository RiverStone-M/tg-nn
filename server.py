import os
import random
import socket
import socketserver
import threading
import re
import logging
import json
import sys
import time
from datetime import datetime

logging.basicConfig(level=logging.DEBUG)

# queues is a dict of lists,each list corresponding to a different test
queues = dict()
counterPassword = 454656
countP = 0
TIMEOUT = 10


class controlHandler(socketserver.BaseRequestHandler):
    def setup(self):
        self._lock = threading.Lock()

    def handle(self):
        while 1:
            self.data = str(self.request.recv(4096).strip().decode("utf-8"))
            if self.data == '':
                break

            self.msg = self.evaluate()

            if self.msg["action"] == 'NewPeer':
                self.caseNewPeer()
            elif self.msg["action"] == 'AskingPeer':
                self.caseAskingPeer()
            elif self.msg["action"] == 'error':
                break
            else:
                break

    def finish(self):
        global queues
        #cleanup goes here
        logging.debug(
            f"client {self.client_address[0]}:{self.client_address[1]} disconnected"
        )
        with self._lock:
            for k in queues.keys():
                for peer in queues[k]:
                    if self.isSamePeer(peer):
                        queues[k].remove(peer)

    def evaluate(self):
        msg = dict()
        try:
            msg = json.loads(self.data)
        except:
            msg["action"] = "error"
        return msg

    def getCurrentPeer(self):
        global queues
        for k in queues.keys():
            for peer in queues[k]:
                if self.isSamePeer(peer):
                    return peer

    def getCurrentTimeStr(self):
        return str(datetime.now().strftime("%Y/%m/%d %H:%M:%S"))

    def getDatetimeFromStr(self, timestring):
        return datetime.strptime(timestring, "%Y/%m/%d %H:%M:%S")

    def refreshLastContact(self, peer):
        peer["lastContact"] = self.getCurrentTimeStr()

    def caseAskingPeer(self):
        global queues
        logging.debug("Peer Asking for pair")

        with self._lock:
            cPeer = self.getCurrentPeer()
            self.refreshLastContact(cPeer)
            self.checkPortChange(cPeer)
            for k in queues.keys():
                for peer in queues[k]:
                    if peer["status"] == "paired":
                        if self.isSamePeer(peer):
                            self.sendTestStatus()
                            break
                        continue
                    else:
                        if self.isSamePeer(peer):
                            continue
                        else:
                            currentPeer = self.getCurrentPeer()
                            if currentPeer == None:
                                continue
                            if currentPeer["testName"] == peer["testName"]:
                                self.pairingPeers(peer)
                                break
                            continue
        self.sendTestStatus()

    def checkPortChange(self, peer):
        if peer["portPublic"] != self.client_address[1]:
            peer["portPublic"] = self.client_address[1]
            logging.debug(
                f"Port changed from client {peer['ipPublic']}:{peer['portPublic']}"
            )

    def cleanInnactivePeers(self):
        global queues
        with self._lock:
            for k in queues.keys():
                for peer in queues[k]:
                    if (datetime.now() - self.getDatetimeFromStr(
                            peer["lastContact"])).seconds > 5:
                        logging.debug(f"removing peer {peer} for inactivity")
                        queues[k].remove(peer)

    #nao precisa de lock,a funcao que chama ja possui o mesmo
    def pairingPeers(self, peer):
        global counterPassword, queues
        counterPassword += random.randint(1, 100000)
        logging.debug("Peer Found,Pairing peers")
        for k in queues.keys():
            for p in queues[k]:

                if self.isSamePeer(p):
                    p["status"] = "paired"
                    p["pairIP"] = peer["ipPublic"]
                    p["pairPORT"] = peer["portPublic"]
                    p["pairNAT"] = peer["natType"]
                    p["Password"] = counterPassword
                    peer["status"] = "paired"
                    peer["pairIP"] = p["ipPublic"]
                    peer["pairPORT"] = p["portPublic"]
                    peer["pairNAT"] = p["natType"]
                    peer["Password"] = counterPassword

    def isSamePeer(self, peer):
        return peer["ipPublic"] == self.client_address[0] and peer[
            "portPublic"] == self.client_address[1]

    #nao precisa de lock,a funcao que chama ja o possui
    def sendTestStatus(self):
        global queues
        message = ""
        for k in queues.keys():
            for peer in queues[k]:
                if self.isSamePeer(peer):
                    message = json.dumps(peer)
                    if peer["status"] == "paired":
                        queues[k].remove(peer)
                    break
        self.request.sendall(bytes(message, "utf-8"))

    def caseNewPeer(self):
        #check for client-perceived address
        self.cleanInnactivePeers()
        logging.debug("new peer being added to list")
        with self._lock:
            global queues
            if queues.get(self.msg["testName"]) == None:
                queues[self.msg["testName"]] = list()

            newpeer = dict()
            newpeer["ipPublic"] = self.client_address[0]
            newpeer["portPublic"] = self.client_address[1]
            newpeer["natType"] = self.msg["natType"]
            newpeer["status"] = "waiting"
            newpeer["testName"] = self.msg["testName"]
            newpeer["lastContact"] = self.getCurrentTimeStr()
            queues[self.msg["testName"]].append(newpeer)
            logging.debug(queues)
            message = json.dumps(newpeer)
            self.request.sendall(bytes(message, "utf-8"))


def initServer(port):

    SERVER_HOST = socket.gethostname()
    #SERVER_IP = socket.gethostbyname(SERVER_HOST)
    #ABAIXO A OPCAO PARA TESTES NO MEU PC COM O MODOD NAT NETWORK DO VIRTUAL BOX(NO CASO SERIA O IP DA MAQUINA)
    SERVER_IP = "192.168.0.13"
    SERVER_PORT = port
    controlSocket = server((SERVER_IP, SERVER_PORT), controlHandler)
    controlSocket.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    logging.debug("host:{}\nIP:{}".format(SERVER_HOST, SERVER_IP))
    while 1:
        controlSocket.handle_request()


class server(socketserver.ThreadingTCPServer):
    def __init__(self, server_address, RequestHandlerClass):
        socketserver.ThreadingTCPServer.__init__(self, server_address,
                                                 RequestHandlerClass)


def Main():
    try:
        port = int(sys.argv[1])

    except:
        print("usage: server.py PORT_NUMBER")

    initServer(port)


Main()
