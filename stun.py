import subprocess
import logging
logging.basicConfig(level=logging.DEBUG)

def getExtAddress():
	logging.debug("Finding NAT type using STUN.Will take a few seconds.")
	process= subprocess.Popen(["pystun"],stdout=subprocess.PIPE)
	
	output,error = process.communicate()

	if error :
		logging.debug("{},NAT type unnavailable,quiting program".format(error))
		quit()


	return output.decode("utf-8")



'''
chama um script em python 2 que usa pystun para descobrir o tipo de NAT
'''
def getExtAddress1():
	logging.debug("Finding NAT type using STUN.Will take a few seconds.")
	process= subprocess.Popen(["python2","netStun.py"],stdout=subprocess.PIPE)
	
	output,error = process.communicate()

	if error :
		logging.debug("{},NAT type unnavailable,quiting program".format(error))
		quit()


	return output.decode("utf-8").strip().split(",")

