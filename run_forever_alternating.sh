#!/bin/bash

# user dependant variables

INTERNAL_PORT=60000   # port on your machine
EXTERNAL_PORT=60000   # port on your router that will be mapped to your interior port
SERVER_IP=10.0.2.4    # peerfinder server's address. IP da UFRGS:143.54.85.22
SERVER_PORT=10000     # peerfinder server's port. porta da UFRGS:12989
INTERFACE=enp0s3      # your layer 2 interface. Only needed for tshark. Ignore if not using tshark

# non user related variables

FIXED_AMOUNT_OF_SECS=80  
TEST_ALTERNATOR=1
TEST_ATTEMPTS=0

# upnpc is commented because im running with portmapping already defined, remem
# upnpc -a $INTERNAL_IP $INTERNAL_PORT $EXTERNAL_PORT TCP $FIXED_AMOUNT_OF_SECS
logFilename=logs_$(date -d "today" +"%Y-%m-%d_%H-%M-%S").txt     #The date just signifies that the capture started at certain date,the tests will/can be run for many days 
touch $logFilename
# tshark not really needed
# tshark -i ${INTERFACE} -a duration:${FIXED_AMOUNT_OF_SECS} > ${captureFilename} &


while :
do
    SECS_SLEEP=$[$RANDOM % 60 + $FIXED_AMOUNT_OF_SECS]            # random part is needed for the special case of having very few peers , if everybody slept for a fixed amount of time it would be possible for them to never connect.
    
    TEST_ATTEMPTS=$[$TEST_ATTEMPTS + 1]
    
    echo test attempt number $TEST_ATTEMPTS ,date: $(date -d "today" +"%Y-%m-%d_%H-%M-%S") >> $logFilename

    echo running client for $FIXED_AMOUNT_OF_SECS seconds

    if [ $TEST_ALTERNATOR -eq 1 ]; then
    	timeout -s 9 ${FIXED_AMOUNT_OF_SECS}s python3 client.py --serverPort $SERVER_PORT --serverIP $SERVER_IP --myPort $INTERNAL_PORT -a -da PingPong >> $logFilename 2>&1
    	TEST_ALTERNATOR=0
    else 
    	timeout -s 9 ${FIXED_AMOUNT_OF_SECS}s python3 client.py --serverPort $SERVER_PORT --serverIP $SERVER_IP --myPort $INTERNAL_PORT -a -da iperf >> $logFilename 2>&1
    	TEST_ALTERNATOR=1
    fi
    fuser -k ${INTERNAL_PORT}/tcp
    echo sleeping for $SECS_SLEEP seconds
    sleep $SECS_SLEEP
done