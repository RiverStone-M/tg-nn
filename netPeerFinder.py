import socket
import json
import logging
import time 

def setupSock(PORT):
    sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    sock.bind(('',PORT))
    
    return sock

#ideally for this method and the findPeer method i should create a GenerateMsg class to generate the json str according to the message that i want to send,but i think since the communication is so simple this way is fine as it is.    
def PeerFinderConnect(sock,serverIP,serverport,natType,testName):
    sock.connect((serverIP,int(serverport)))
    sendmsg=dict()
    sendmsg["action"]="NewPeer"
    sendmsg["natType"]=natType
    sendmsg["testName"]=testName
    sendmessage=str()
    try:
        sendmessage=json.dumps(sendmsg)
    except Exception as e:
        print(e)
        raise Exception("json fail")
    
    sock.sendall(bytes(sendmessage,"utf-8"))
    response=sock.recv(1024*4).decode("utf-8")
    try:
        msg=json.loads(response)
        logging.debug(msg)
        if msg["status"]=="waiting":
            return "ok"
        else:
            raise Exception("broken message")
    except Exception as e:
        print(e)
        raise Exception("json fail")
    
def findPeer(sock):
    msg=dict()
    msg["action"]="AskingPeer"
    sendmsg=json.dumps(msg)
    while 1:
        sock.sendall(bytes(sendmsg,"utf-8"))
        msg2=sock.recv(1024*4).decode("utf-8")

        try:
            response=json.loads(msg2)
            if response["status"]=="paired":
                return response
        except:
            return "error"
        
        logging.debug("waiting still...")
        time.sleep(1)

    return "error"



    