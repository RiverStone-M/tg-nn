### ABOUT ###

This is my Undergraduate project about Network Neutrality tests between peers in a network.
It uses a centralized P2P network to realize network tests.
The server's purpose is to give peers adresses of other peers that want to execute a certain test.
Then, the peers can execute 1 of 2 possible premade tests or use a custom test.

### DEPENDENCIES ###

Not required,but better results can be obtained if miniupnpc and tshark are installed.

* miniupnpc is used for port mapping, makes connecting peers a little easier
* tshark is used for capturing packets, usefull for finding instances where "random" TCP RST packets are being received

### USAGE ###

Change the variables in run_forever.sh or run_forever_alterning.sh to your home parameters.The comments should guide you.

example in terminal: $ bash ./run_forever_alternating.sh

### TODO ###

* make script to convert the logs into data
* run tests to generate the dataset
* write actual thesis
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
