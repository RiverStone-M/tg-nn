#!/bin/bash
# DONT USE THIS ONE ! USE run_forever_alternating.sh INSTEAD !
# this was the old testing script. I may use a modified version of it again if a script to run only one test is needed


INTERNAL_PORT=60000
EXTERNAL_PORT=60000
INTERNAL_IP=192.168.1.13
PORT_MAPPING_TIME=60
SERVER_IP=143.54.85.22
SERVER_PORT=12989

while :
do
    RTI=$[$RANDOM % 30 + 20]
    upnpc -a $INTERNAL_IP $INTERNAL_PORT $EXTERNAL_PORT TCP $PORT_MAPPING_TIME
    echo running client for $PORT_MAPPING_TIME seconds
    timeout -s 9 ${PORT_MAPPING_TIME}s python3 client.py --serverPort $SERVER_PORT --serverIP $SERVER_IP --myPort $INTERNAL_PORT -a
    fuser -k ${INTERNAL_PORT}/tcp
    echo sleeping for $RTI seconds
    sleep $RTI
done