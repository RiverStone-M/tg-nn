import socket
import sys
import time
import threading
import logging
import json
import random
import os
import datetime
import re
from customTest import *

MAX_RANDOM_GUESS = 50_063_860
logging.basicConfig(level=logging.DEBUG)

myState = "undefined"
myGuess = 0
imFinished = False
peerFinished = False
imConnectedWithServer = False
PeerConnectedWithMyServer = False
stopServer = False
stopClient = False


def ABORT():
    global stopClient, stopServer
    stopServer = True
    stopClient = True


class Server(threading.Thread):
    def __init__(self, port):
        super(Server, self).__init__()
        self._lock = threading.Lock()
        self.port = port
        self.pingpongs = list()

    def run(self):
        global imFinished, peerFinished, PeerConnectedWithMyServer, stopServer

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.bind(('', self.port))
        except Exception as e:
            print(e)
            raise Exception("bind failed")
            self.sock.close()
            exit()

        self.sock.listen()
        logging.debug(f"Listening on port {self.port}\n")

        if stopServer == True:
            return

        (self.clientSocket, self.clientAddress) = self.sock.accept()

        with self._lock:
            PeerConnectedWithMyServer = True

        logging.debug(f"Connection from {str(self.clientAddress)}\n")

        while 1:
            if (imFinished and peerFinished) or stopServer == True:
                break

            self.rcvMsg = str(
                self.clientSocket.recv(4096).strip().decode("utf-8"))
            self.rcvMsg = self.evaluate()
            if self.rcvMsg["action"] == 'definingSender':
                self.definingSender()
            elif self.rcvMsg["action"] == 'PingPong':
                self.PingPong()
            elif self.rcvMsg["action"] == 'iperf':
                self.iperfT()
            elif self.rcvMsg["action"] == 'custom':
                responderSide(self.clientSocket)
            elif self.rcvMsg["action"] == 'error':
                logging.debug(f"Received invalid action {self.rcvMsg}")
                ABORT()
                break
            else:
                continue

    def evaluate(self):
        msg = dict()
        try:
            msg = json.loads(self.rcvMsg)
        except:
            logging.debug(self.rcvMsg)
            msg["action"] = "error"
        return msg

    #provavelmente a fun'ao mais delicada,sempre tenho que considerar que o peer eh server e cliente ao mesmo tempo
    #logo,os peers vao tentar definir o sender na parte de server e de client
    #tambem tenho que considerar o caso onde so um dos peers conseguiu connectar no server
    #tomar cuidado com o lock e com as variaveis globais
    #tenho que considerar que o chute pode ter sido o mesmo para os dois
    #Eu consegui deixar essa funcao um pouco menos delicada.agora quem eh server ou client eh definido pela porta usada.
    def definingSender(self):
        global MAX_RANDOM_GUESS, myState, myGuess, stopServer, stopClient, imConnectedWithServer, PeerConnectedWithMyServer
        logging.debug("definingSender")

        if imConnectedWithServer:
            if myState == "undefined":
                if myGuess == 0:
                    with self._lock:
                        myGuess = random.randint(1, MAX_RANDOM_GUESS)
                if int(self.rcvMsg["guess"]) > myGuess:
                    self.sendSenderState("Client")
                    with self._lock:
                        myState = "Server"
                else:
                    self.sendSenderState("Server")
                    with self._lock:
                        myState = "Client"
            if myState == "Client":
                self.sendSenderState("Server")
            else:
                self.sendSenderState("Client")
        else:
            with self._lock:
                myState = "Server"
            self.sendSenderState("Client")

        if myState == "Server":

            stopClient = True
        else:

            stopServer = True

        logging.debug("exiting definingSender")

    def sendSenderState(self, state):
        msg = dict()
        msg["action"] = "definingSender"
        msg["state"] = state
        message = json.dumps(msg)
        self.clientSocket.sendall(bytes(message, "utf-8"))

    def saveResults(self, testName, result):
        logging.debug(f"saving results of {testName}")
        results = dict()
        results["test"] = testName
        results["results"] = result
        results["date"] = str(datetime.datetime.now())
        finalRes = json.dumps(results)

        try:
            f = open(f"results{testName}{str(datetime.datetime.now())}.txt", "w+")
            f.write(finalRes)
        except Exception as e:
            logging.debug(type(e))
            logging.debug(e)
        finally:
            f.close()

    def closeConnection(self):
        logging.debug("Finished testing,closing connection.Responder")
        self.clientSocket.close()
        self.sock.close()
        ABORT()

    def PingPong(self):
        #logging.debug(f"received {self.rcvMsg}")
        self.rcvMsg["date"] = str(datetime.datetime.now())
        self.pingpongs.append(self.rcvMsg)
        if self.rcvMsg["direction"] == "normal":
            #logging.debug("inside pingpong normal direction")
            msg = dict()
            msg["action"] = "PingPong"
            message = json.dumps(msg)
            self.clientSocket.sendall(bytes(message, "utf-8"))
        if self.rcvMsg["direction"] == "reverse":
            msg = dict()
            msg["action"] = "PingPong"
            msg["subAction"] = "reversing"
            message = json.dumps(msg)
            self.clientSocket.sendall(bytes(message, "utf-8"))
            resp = self.clientSocket.recv(1024 * 4)

            for i in range(0, 10):  #TODO:trocar para "tries"
                self.clientSocket.sendall(bytes(message, "utf-8"))
                resp = self.clientSocket.recv(1024 * 4).strip().decode("utf-8")
                resp=json.loads(resp)
                #logging.debug(f"resp inside for pintpong={resp}")
                if resp["subAction"]=="results":
                    self.saveResults("PingPong",resp["results"])
                    
                if i == 9:
                    msg = dict()
                    msg["action"] = "PingPong"
                    msg["subAction"] = "finish"
                    message = json.dumps(msg)
                    logging.debug("testFinished")
                    

                    self.clientSocket.sendall(bytes(message, "utf-8"))
                    self.closeConnection()
                    break

        


    def iperfT(self):
        logging.debug("inside iperf responder")
        jsonRE = re.compile("[{][^}{]+[}]")
        totalLen = 0
        finished = False
        if self.rcvMsg["subAction"] == "start":
            self.mirrorMsg()
        while not finished:
            resp = self.clientSocket.recv(4096).strip()
            totalLen += len(resp)
            try:
                resp = str(resp.decode("utf-8"))
                match = jsonRE.search(resp)
                if match:
                    logging.debug(f"found match! {match.group(0)}")
                    self.rcvMsg = json.loads(match.group(0))
                    finished = True
                    self.rcvMsg["bytesReceived"] = totalLen
                    self.mirrorMsg()

                if self.rcvMsg["subAction"] == "finish":
                    finished = True
                    self.rcvMsg["bytesReceived"] = totalLen
                    self.mirrorMsg()
            except:
                pass

        resp = self.clientSocket.recv(4096).strip().decode("utf-8")
        logging.debug(f"just before reverse msg={self.rcvMsg}")
        self.rcvMsg = json.loads(resp)
        

        if self.rcvMsg["subAction"] == "reverse":
            bunchOBytes = bytes("f" * 500, "utf-8")

            for i in range(10000):
                self.clientSocket.sendall(bunchOBytes)
            msend = dict()
            msend["action"] = "iperf"
            msend["subAction"] = "finishResponder"
            msendj = json.dumps(msend)
            self.clientSocket.sendall(bytes(msendj, "utf-8"))
            resp = self.clientSocket.recv(4096).strip().decode("utf-8")
            self.rcvMsg = json.loads(resp)
            self.saveResults("iperf",self.rcvMsg["results"])

            logging.debug("Iperf Test Reverse direction finished")

        self.closeConnection()

    def mirrorMsg(self):
        msg = json.dumps(self.rcvMsg)
        self.clientSocket.sendall(bytes(msg, "utf-8"))


class Client(threading.Thread):
    def __init__(self, myIP, myPort, peerAdress, testChosen, tries=10):
        super(Client, self).__init__()
        self.myIP = myIP
        self.port = myPort
        self.peerIP = peerAdress[0]
        self.peerPort = peerAdress[1]
        self.testChosen = testChosen
        self.tries = tries
        self.pingpongCompleted = False
        self._lock = threading.Lock()

    def connect(self):
        global myState, stopClient, stopServer
        connected = 0
        for trie in range(0, self.tries):
            try:
                if myState != "undefined":
                    connected = 1

                self.sock.connect((self.peerIP, self.peerPort))
                logging.debug(
                    f"Succesful Connection attempt with {str(self.peerIP)}:{str(self.peerPort)}"
                )
                return
            except:
                logging.debug(
                    f"Connection attempt #{str(trie+1)} with {str(self.peerIP)}:{str(self.peerPort)} failed"
                )
                time.sleep(6)
        self.saveResults("FailedToConnect","FailedToConnect")
        with self._lock:
            UNEXPECTED_BEHAVIOR_ABORT()
        raise Exception("Unable to connect")

    def closeConnection(self):
        global imFinished,peerFinished

        with self._lock:
            imFinished = True
            peerFinished = True

        logging.debug("Finished testing,closing connection")
        self.sock.close()
        ABORT()
    

    def tryBinding(self):
        try:
            logging.debug("Attempting binding")
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.bind(('', self.port))
            logging.debug("Requester binding ok")
        except Exception as e:
            logging.debug("Binding Attempt failed")
            logging.debug(e)

    def run(self):
        global imFinished, peerFinished, myState, imConnectedWithServer, stopClient, stopServer

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #now this operation is inside try binding. I may change it back to here
        #self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.settimeout(10)
        
        self.tryBinding()

        try:
            self.connect()
            with self._lock:
                imConnectedWithServer = True
        except Exception as e:
            logging.debug(e)
            return 2

        while 1:
            if imFinished and peerFinished:
                logging.debug("client exiting")
                stopClient = True
                break
            self.defineFirstSender()
            if myState == "Client":
                self.runTest()

                self.closeConnection()
            else:
                time.sleep(0.5)

        self.sock.close()
        return 1

    def runTest(self):
        if self.testChosen == "PingPong":
            self.PingPongTest()
        if self.testChosen == "iperf":
            self.iperfTest()
        if self.testChosen == "custom":
            self.customTest()
        else:
            pass

    #send 5MB in one direction then does it again in the reverse direction
    def iperfTest(self):
        result = list()
        msend = dict()
        msend["action"] = "iperf"
        msend["subAction"] = "start"
        msend["data"] = "f" * 5
        msendj = json.dumps(msend)
        self.sock.sendall(bytes(msendj, "utf-8"))
        bunchOBytes = bytes("f" * 500, "utf-8")
        response = self.sock.recv(1024 * 4).decode("utf-8")
        if json.loads(response)["subAction"] == "start":
            tic = time.perf_counter()
            for i in range(10000):
                self.sock.sendall(bunchOBytes)
            msend["subAction"] = "finish"
            msendj = json.dumps(msend)
            for i in range(3):
                self.sock.sendall(bytes(msendj, "utf-8"))
            resp = self.sock.recv(1024 * 4).strip().decode("utf-8")
            toc = time.perf_counter()
            tictoc = toc - tic
            resX = json.loads(resp)
            res = dict()
            res["direction"] = "requester->responder"
            res["timeElapsed"] = tictoc
            res["from"] = f"{self.myIP}:{self.port}"
            res["to"] = f"{self.peerIP}:{self.peerPort}"
            res["bytesReceived"] = resX["bytesReceived"]
            result.append(res)
            
            #beggining reversed stage
            msend = dict()
            msend["action"] = "iperf"
            msend["subAction"] = "reverse"
            msend["data"] = "f" * 5
            msendj = json.dumps(msend)
            self.sock.sendall(bytes(msendj, "utf-8"))
            
            finished = False
            jsonRE=re.compile("[{][^}{]+[}]")
            totalLen=0
            tic=time.perf_counter()
            recv=dict()
            while not finished:
                resp = self.sock.recv(4096).strip()
                totalLen += len(resp)
                try:
                    resp = str(resp.decode("utf-8"))
                    match = jsonRE.search(resp)
                    if match:
                        logging.debug(f"found match! {match.group(0)}")
                        recv = json.loads(match.group(0))

                    if recv["subAction"] == "finishResponder":
                        logging.debug("inside finished")
                        finished = True
                        recv["bytesReceived"] = totalLen
                        
                except:
                    pass
            toc = time.perf_counter()
            res = dict()
            res["direction"] = "responder->requester"
            res["timeElapsed"] = tictoc
            res["requester"] = f"{self.myIP}:{self.port}"
            res["resqponder"] = f"{self.peerIP}:{self.peerPort}"
            res["bytesReceived"] = recv["bytesReceived"]
            result.append(res)
            newRes=dict()
            newRes["results"]=result
            resJson=json.dumps(newRes)
            self.sock.sendall(bytes(resJson, "utf-8"))
            
            self.saveResults("iperf",result)

    def customTest(self):
        requesterSide(self.sock)



    def defineFirstSender(self):
        global MAX_RANDOM_GUESS, myState, myGuess
        logging.debug("entering defineFirstSender")
        if myState == "undefined":
            with self._lock:
                myGuess = random.randint(1, MAX_RANDOM_GUESS)
            myStateX = self.getStateFromServer()
            with self._lock:
                myState = myStateX
                self.setDaemonStops()
        logging.debug("exiting defineFirstSender")

    def setDaemonStops(self):
        global myState, stopClient, stopServer
        if myState == "Server":
            stopClient = True

        else:
            stopServer = True

    def getStateFromServer(self):
        global myGuess, myState
        msg = dict()
        msg["action"] = "definingSender"
        msg["guess"] = str(myGuess)
        message = json.dumps(msg)
        self.sock.sendall(bytes(message, "utf-8"))

        response = self.sock.recv(1024 * 4).decode("utf-8")
        msg = json.loads(response)
        if msg["state"] == "Client" or msg["state"] == "Server":
            return msg["state"]
        else:
            return "undefined"

    def PingPongTest(self, pingpongs=5):
        logging.debug("PingPongTest")
        result = list()
        for ping in range(0, pingpongs):
            pingD = dict()
            msg = dict()
            msg["action"] = "PingPong"
            msg["direction"] = "normal"
            message = json.dumps(msg)
            tic = time.perf_counter()
            self.sock.sendall(bytes(message, "utf-8"))
            response = self.sock.recv(1024 * 4).decode("utf-8")
            toc = time.perf_counter()
            tictoc = toc - tic
            rcvMsg = json.loads(response)
            if rcvMsg["action"] == "PingPong":
                pingD["Direction"] = "requester->responder"
                pingD["from"] = f"{self.myIP}:{self.port}"
                pingD["to"] = f"{self.peerIP}:{self.peerPort}"
                pingD["RTT in seconds"] = f"{tictoc}"
            else:
                pingD["warning"] = "Error Ocurred"
                pingD["Direction"] = "requester->responder"
                pingD["from"] = f"{self.myIP}:{self.port}"
                pingD["to"] = f"{self.peerIP}:{self.peerPort}"
                pingD["RTT in seconds"] = f"{tictoc}"
            result.append(pingD)

        finished = False
        firstReverse = True
        timeToSendResults=False
        i=0
        while not finished:
            pingD = dict()
            msg = dict()
            msg["action"] = "PingPong"
            msg["direction"] = "reverse"
            msg["subAction"] = "reversing"
            message = json.dumps(msg)
            
            if firstReverse == True:
                tic = time.perf_counter()
                self.sock.sendall(bytes(message, "utf-8"))
                response = self.sock.recv(1024 * 4).decode("utf-8")
                toc = time.perf_counter()
                self.sock.sendall(bytes(message, "utf-8"))
                tictoc = toc - tic
                tic = time.perf_counter()
                logging.debug("client response reversed")
                firstReverse = False
                logging.debug(response)
            elif timeToSendResults == True:

                response = self.sock.recv(1024 * 4).decode("utf-8")
                toc = time.perf_counter()
                tictoc = toc - tic
                msg["subAction"]="results"
                msg["results"]=result
                message = json.dumps(msg)
                self.sock.sendall(bytes(message, "utf-8"))
                tic = time.perf_counter()
                timeToSendResults=False
            else:
                response = self.sock.recv(1024 * 4).decode("utf-8")
                toc = time.perf_counter()
                tictoc = toc - tic
                self.sock.sendall(bytes(message, "utf-8"))
                tic = time.perf_counter()
                
            rcvMsg = json.loads(response)

            if rcvMsg["subAction"] == "finish":
                finished = True
            

            pingD["Direction"] = "responder->requester"
            pingD["to"] = f"{self.myIP}:{self.port}"
            pingD["from"] = f"{self.peerIP}:{self.peerPort}"
            pingD["RTT in seconds"] = f"{tictoc}"
            result.append(pingD)
            i=i+1
            if i == 8:
                timeToSendResults=True

            

        self.saveResults("PingPong", result)

    def saveResults(self, testName, result):
        #logging.debug(f"saving results of {testName} in file {testName}.txt")
        results = dict()
        results["test"] = testName
        results["myAddress"] = f"{self.myIP}:{self.port}"
        results["targetAddress"] = f"{self.peerIP}:{self.peerPort}"
        results["results"] = result
        results["date"] = str(datetime.datetime.now())
        finalRes = json.dumps(results)

        try:
            f = open(f"results{testName}{str(datetime.datetime.now())}.txt", "w+")
            f.write(finalRes)
        except Exception as e:
            logging.debug(type(e))
            logging.debug(e)
        finally:
            f.close()

def PeerCommunication(myIP, myPort, peerAdress, testChosen):
    global stopClient, stopServer
    
    if myPort < peerAdress[1]:
        print("Starting requester part of the peer")
        cli = Client(myIP, myPort, peerAdress, testChosen)
        cli.daemon = True
        print("Started successfully")
        cli.start()
        srv = Server(myPort)

        srv.daemon = True
        print("Starting responder part of the peer")
        srv.start()
    else:
        srv = Server(myPort)

        srv.daemon = True
        print("Starting responder part of the peer")
        srv.start()
        print("Starting requester part of the peer")
        cli = Client(myIP, myPort, peerAdress, testChosen)
        cli.daemon = True
        print("Started successfully")
        cli.start()
    
    time.sleep(1)

    while True:

        if stopServer and stopClient:
            break
        time.sleep(1)


if __name__ == '__main__':
    try:
        test = sys.argv[1]
        test2 = sys.argv[2]
        test3 = sys.argv[3]
        test4 = sys.argv[4]
    except:
        print("usage:netPeerTests.py myIP myPort peerAdress peerPort")
        quit()

    if sys.argv[2] == '--localhost' or sys.argv[2] == '-l':
        peerIP = socket.gethostbyname(socket.gethostname())

    PeerCommunication(sys.argv[1], int(sys.argv[2]),
                      [peerIP, int(sys.argv[4])])
