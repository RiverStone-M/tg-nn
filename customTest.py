import json
import socket
import time

customTestName = "PingPongCustom"

def requesterSide(requesterSocket):
    #logging.debug("PingPongTest")
    print("PingPongCustom Test")
    result=list()
    for ping in range(0,5):
        pingD=dict()
        msg=dict()
        msg["action"]="custom"
        message=json.dumps(msg)
        tic=time.perf_counter()
        requesterSocket.sendall(bytes(message,"utf-8"))
        response=requesterSocket.recv(1024*4).decode("utf-8")
        toc=time.perf_counter()
        tictoc=toc-tic
        rcvMsg=json.loads(response)
        if rcvMsg["action"]=="PingPongCustom":
            pingD["Direction"]="client->server"
            pingD["RTT in seconds"]=f"{tictoc}"
        else:
            pingD["warning"]="Error Ocurred"
            pingD["Direction"]="client->server"
            pingD["RTT in seconds"]=f"{tictoc}"
        result.append(pingD)
    print(result)


def responderSide(responderSocket):
    msg=dict()
    msg["action"]="PingPongCustom"
    message=json.dumps(msg)
    responderSocket.sendall(bytes(message,"utf-8"))